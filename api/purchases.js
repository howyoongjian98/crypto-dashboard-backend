'use strict'

const AWS = require('aws-sdk')
const uuid = require('uuid')


AWS.config.setPromisesDependency(require('bluebird'))

const dynamoDb = new AWS.DynamoDB.DocumentClient()

// record purchase
module.exports.create = (event, context, callback) => {
  const { email, purchases, total, currency } = JSON.parse(event.body)

  if (typeof email !== 'string' || typeof purchases !== 'object' || typeof total !== 'number' || typeof currency !== 'string') {
    console.error('Validation Failed')
    callback(new Error('Couldn\'t record purchase because of validation errors.'))
    return
  }

  recordPurchase(purchaseInfo(purchases, email, total, currency))
    .then(res => {
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,POST"
        },
        body: JSON.stringify({
          message: `Sucessfully recorded purchase with ${email}`,
          purchaseId: res.id
        })
      })
    })
    .catch(err => {
      console.log(err)
      callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: `Unable to record purchase with ${email}`
        })
      })
    })
}


const recordPurchase = purchase => {
  const purchaseInfo = {
    TableName: process.env.PURCHASE_TABLE,
    Item: purchase,
  }
  return dynamoDb.put(purchaseInfo).promise()
    .then(() => purchase)
}

const purchaseInfo = (purchases, email, total, currency) => {
  const timestamp = new Date().getTime()
  return {
    id: uuid.v1(),
    purchases,
    email,
    currency,
    totalPrice: total,
    submittedAt: timestamp,
    updatedAt: timestamp,
  }
}

// list purchases
module.exports.list = (event, context, callback) => {
  var params = {
    TableName: process.env.PURCHASE_TABLE,
    ProjectionExpression: "email, purchases, totalPrice, submittedAt, currency",
    FilterExpression: "#email = :email",
    ExpressionAttributeNames: {
      "#email": "email"
    },
    ExpressionAttributeValues: {
      ":email": event.pathParameters.email
    }
  }

  console.log(`Scanning ${process.env.PURCHASE_TABLE} table.`)
  const onScan = (err, data) => {

    if (err) {
      console.log('Scan failed to load data. Error JSON:', JSON.stringify(err, null, 2))
      callback(err)
    } else {
      console.log("Scan succeeded.")
      return callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,GET"
        },
        body: JSON.stringify({
          purchases: data.Items
        })
      })
    }

  }

  dynamoDb.scan(params, onScan)

}