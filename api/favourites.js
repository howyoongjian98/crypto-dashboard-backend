'use strict'

const AWS = require('aws-sdk')


AWS.config.setPromisesDependency(require('bluebird'))

const dynamoDb = new AWS.DynamoDB.DocumentClient()


module.exports.favouriteCoins = (event, context, callback) => {
  const { favourites } = JSON.parse(event.body)

  if (!Array.isArray(favourites)) {
    console.error('Validation Failed')
    callback(new Error('Couldn\'t record purchase because of validation errors.'))
    return
  }

  const params = {
    TableName: process.env.FAVOURITES_TABLE,
    Key: {
      "email": event.pathParameters.email
    },
    UpdateExpression: "set favourites = :favourites",
    ExpressionAttributeValues: {
      ":favourites": favourites,
    },
    ReturnValues: "UPDATED_NEW"
  }

  const onUpdate = (err, data) => {
    if (err) {
      console.log('Failed to udpate favourites. Error JSON:', JSON.stringify(err, null, 2))
      callback(err)
    } else {
      console.log("Update succeeded.")
      return callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,PATCH"
        },
        body: JSON.stringify({
          favourites: data.Attributes.favourites
        })
      })
    }
  }

  dynamoDb.update(params, onUpdate)
}

module.exports.listFavouriteCoins = (event, context, callback) => {
  var params = {
    TableName: process.env.FAVOURITES_TABLE,
    ProjectionExpression: "favourites",
    Key: {
      "email": event.pathParameters.email
    }
  }

  console.log(`Scanning ${process.env.FAVOURITES_TABLE} table.`)
  const onScan = (err, data) => {

    if (err) {
      console.log('Scan failed to load data. Error JSON:', JSON.stringify(err, null, 2))
      callback(err)
    } else {
      console.log("Scan succeeded.")
      return callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,GET"
        },
        body: JSON.stringify({
          favourites: data.Item.favourites
        })
      })
    }

  }

  dynamoDb.get(params, onScan)

}